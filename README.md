# Side Projects

A side project repository full of random experiments.

## What's in this repository?

Sample code, examples, and random experiements. All of the code in these projects may lack polish. 
These experiments are created ad-hoc and may have serious logic errors or show-stopping bugs. This 
repository is intended for random experimentation.


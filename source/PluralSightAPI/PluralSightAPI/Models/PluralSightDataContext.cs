namespace PluralSightAPI.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PluralSightDataContext : DbContext
    {
        public PluralSightDataContext()
            : base("name=PluralSightDataContext")
        {
        }

        public virtual DbSet<PluralSightCourse> PluralSightCourses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PluralSightCourse>()
                .Property(e => e.Description)
                .IsUnicode(false);
        }
    }
}

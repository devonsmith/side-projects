namespace PluralSightAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PluralSightCourses")]
    public partial class PluralSightCourse
    {
        public int ID { get; set; }

        [StringLength(255)]
        [Display(Name ="Course ID")]
        public string CourseID { get; set; }

        [StringLength(255)]
        [Display(Name = "Course Title")]
        public string CourseTitle { get; set; }

        [Display(Name = "Duration")]
        public int? DurationInSeconds { get; set; }

        [Column(TypeName = "text")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Release Date")]
        public DateTime? ReleaseDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Assessment Status")]
        public string AssessmentStatus { get; set; }

        public bool IsCourseRetired { get; set; }
    }
}

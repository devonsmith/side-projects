﻿CREATE TABLE PluralSightCourses(
	ID INT IDENTITY(0,1) PRIMARY KEY,
	CourseID NVARCHAR(255),
	CourseTitle NVARCHAR(255),
	DurationInSeconds INT,
	Description TEXT,
	ReleaseDate DATE,
	AssessmentStatus NVARCHAR(50),
	IsCourseRetired BIT
);

SELECT * FROM PluralSightCourses;
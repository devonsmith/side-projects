﻿using PluralSightAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PluralSightAPI.Controllers
{
    public class HomeController : Controller
    {
        PluralSightDataContext db = new PluralSightDataContext();

        // GET: Home
        public ActionResult Index()
        {
            return View(db.PluralSightCourses.ToList());
        }

        public ActionResult Update()
        {
            // Make a web request to get the list of classes.
            WebRequest request = HttpWebRequest.Create("http://api.pluralsight.com/api-v0.9/courses");
            // Create a list to store PluralSightCourse objects.
            List<PluralSightCourse> courses = new List<PluralSightCourse>();
            // Counter for the number of records/courses available from PluralSight.
            int recordCount = 0;

            // Using a web response process the file returned from the API.
            using (WebResponse response = request.GetResponse())
            {
                // Get the file stream and create a stream reader.
                Stream fileStream = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(fileStream);
                // A string to store the line from the file in fileStream.
                string line = null;

                // read all the lines in the file and turn them into PluralSightCourse objects.
                while ((line = streamReader.ReadLine()) != null)
                {
                    /* Get the elements from the line in the file and split them on a comma
                     * that is not in a pair of quotes. Vetted with Regexr */
                    Regex expression = new Regex(',' + "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                    // create a temporary array to get the fields from the CSV file.
                    string[] tempArray = expression.Split(line);

                    // Skip the header in the file. You could also just skip the first line.
                    if (tempArray[0].Equals("CourseId", StringComparison.Ordinal) == false)
                    {
                        /* Build the new PluralSightCourse object from the information in
                           temp array. */
                        PluralSightCourse course = new PluralSightCourse();
                        course.CourseID = tempArray[0];
                        course.CourseTitle = tempArray[1];
                        course.DurationInSeconds = Convert.ToInt32(tempArray[2]);
                        course.ReleaseDate = Convert.ToDateTime(tempArray[3]);
                        course.Description = tempArray[4];
                        course.AssessmentStatus = tempArray[5];
                        course.IsCourseRetired =
                            (tempArray[6].Equals("yes", StringComparison.OrdinalIgnoreCase)) ? true : false;
                        // Once the object is populated, place it into the collection of courses.
                        recordCount++;
                        courses.Add(course);
                    }
                }

            }
            /* Once we've got the list of all the new items on the website, we clear out the database for
               the updated data. */

            // pass through the SQL command to truncate the table, processing this using the EntityFramework and
            // LINQ is not recommended for databases that contain more than 1000 rows.
            db.Database.ExecuteSqlCommand("TRUNCATE TABLE PluralSightCourses");

            // Now that the PluralSight database table is empty we can populate it with the new database
            // information.
            foreach (var course in courses)
            {
                // Add the item to the database.
                // Try it, but don't assume that everything will be OK.
                try
                {
                    db.PluralSightCourses.Add(course);
                }
                catch (Exception e)
                {
                    //TODO: Maybe make a user-facing error for the website. 
                    Debug.WriteLine(e.ToString());
                }
            }
            // Now that items have been added to the DBSet we can save them to the database.
            try
            {
                db.SaveChanges();
            }
            // Catch any exception that might be thrown and allow execution to continue.
            catch (DbEntityValidationException e)
            {
                Debug.WriteLine(e.ToString());
            }
            catch (Exception e)
            {
                //TODO: Maybe make a user-facing error for the website. 
                Debug.WriteLine(e.ToString());
            }
            // Add the number of courses to the viewbag.
            ViewBag.Count = recordCount;
            // Return a view containing the model of the items from the file.
            return View(courses);
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Deli_Menu.Startup))]
namespace Deli_Menu
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

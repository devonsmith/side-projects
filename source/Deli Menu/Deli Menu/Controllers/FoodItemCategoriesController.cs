﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Deli_Menu.Models;

namespace Deli_Menu.Controllers
{
    public class FoodItemCategoriesController : Controller
    {
        private DeliDatabaseContext db = new DeliDatabaseContext();

        // GET: FoodItemCategories
        public ActionResult Index()
        {
            var foodItemCategories = db.FoodItemCategories.Include(f => f.Category).Include(f => f.FoodItem);
            return View(foodItemCategories.ToList());
        }

        // GET: FoodItemCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodItemCategory foodItemCategory = db.FoodItemCategories.Find(id);
            if (foodItemCategory == null)
            {
                return HttpNotFound();
            }
            return View(foodItemCategory);
        }

        // GET: FoodItemCategories/Create
        public ActionResult Create()
        {
            ViewBag.FoodCategoryID = new SelectList(db.Categories, "ID", "CategoryName");
            ViewBag.FoodItemID = new SelectList(db.FoodItems, "ID", "ItemName");
            return View();
        }

        // POST: FoodItemCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FoodItemID,FoodCategoryID")] FoodItemCategory foodItemCategory)
        {
            if (ModelState.IsValid)
            {
                db.FoodItemCategories.Add(foodItemCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FoodCategoryID = new SelectList(db.Categories, "ID", "CategoryName", foodItemCategory.FoodCategoryID);
            ViewBag.FoodItemID = new SelectList(db.FoodItems, "ID", "ItemName", foodItemCategory.FoodItemID);
            return View(foodItemCategory);
        }

        // GET: FoodItemCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodItemCategory foodItemCategory = db.FoodItemCategories.Find(id);
            if (foodItemCategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.FoodCategoryID = new SelectList(db.Categories, "ID", "CategoryName", foodItemCategory.FoodCategoryID);
            ViewBag.FoodItemID = new SelectList(db.FoodItems, "ID", "ItemName", foodItemCategory.FoodItemID);
            return View(foodItemCategory);
        }

        // POST: FoodItemCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FoodItemID,FoodCategoryID")] FoodItemCategory foodItemCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(foodItemCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FoodCategoryID = new SelectList(db.Categories, "ID", "CategoryName", foodItemCategory.FoodCategoryID);
            ViewBag.FoodItemID = new SelectList(db.FoodItems, "ID", "ItemName", foodItemCategory.FoodItemID);
            return View(foodItemCategory);
        }

        // GET: FoodItemCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodItemCategory foodItemCategory = db.FoodItemCategories.Find(id);
            if (foodItemCategory == null)
            {
                return HttpNotFound();
            }
            return View(foodItemCategory);
        }

        // POST: FoodItemCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FoodItemCategory foodItemCategory = db.FoodItemCategories.Find(id);
            db.FoodItemCategories.Remove(foodItemCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

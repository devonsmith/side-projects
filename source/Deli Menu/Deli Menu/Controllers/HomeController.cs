﻿using Deli_Menu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Deli_Menu.Controllers
{
    public class HomeController : Controller
    {
        private DeliDatabaseContext db = new DeliDatabaseContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Menu()
        {
            return View(db.FoodItems.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // Admin Demo View
        public ActionResult Admin()
        {
            return View();
        }

        // GET: Home/Item/5
        public ActionResult Item(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodItem foodItem = db.FoodItems.Find(id);
            if (foodItem == null)
            {
                return HttpNotFound();
            }
            return View(foodItem);
        }
    }
}
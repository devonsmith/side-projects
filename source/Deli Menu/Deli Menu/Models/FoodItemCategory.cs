namespace Deli_Menu.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FoodItemCategory")]
    public partial class FoodItemCategory
    {
        public int ID { get; set; }

        public int? FoodItemID { get; set; }

        public int? FoodCategoryID { get; set; }

        public virtual Category Category { get; set; }

        public virtual FoodItem FoodItem { get; set; }
    }
}

namespace Deli_Menu.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DeliDatabaseContext : DbContext
    {
        public DeliDatabaseContext()
            : base("name=DeliDatabaseContext")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<FoodItem> FoodItems { get; set; }
        public virtual DbSet<FoodItemCategory> FoodItemCategories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(e => e.FoodItemCategories)
                .WithOptional(e => e.Category)
                .HasForeignKey(e => e.FoodCategoryID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<FoodItem>()
                .HasMany(e => e.FoodItemCategories)
                .WithOptional(e => e.FoodItem)
                .WillCascadeOnDelete();
        }
    }
}

namespace Deli_Menu.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FoodItem")]
    public partial class FoodItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FoodItem()
        {
            FoodItemCategories = new HashSet<FoodItemCategory>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string ItemName { get; set; }

        public int ServingSize { get; set; }

        public int ServingsContained { get; set; }

        public int Calories { get; set; }

        public int TotalFat { get; set; }

        public int? SaturatedFat { get; set; }

        public int? TransFat { get; set; }

        public int Colesterol { get; set; }

        public int Sodium { get; set; }

        public int TotalCarbohydrate { get; set; }

        public int DietaryFiber { get; set; }

        public int TotalSugars { get; set; }

        public int? AddedSugars { get; set; }

        public int Protien { get; set; }

        public int? A { get; set; }

        public int? C { get; set; }

        public int? Calcuim { get; set; }

        public int? Iron { get; set; }

        public int? D { get; set; }

        public int? E { get; set; }

        public int? K { get; set; }

        public int? Thiamin { get; set; }

        public int? Ribolavin { get; set; }

        public int? Niacin { get; set; }

        public int? Folate { get; set; }

        public int? B12 { get; set; }

        public int? Biotin { get; set; }

        public int? PantothenicAcid { get; set; }

        public int? Phosphorus { get; set; }

        public int? Iodine { get; set; }

        public int? Magnesium { get; set; }

        public int? Zinc { get; set; }

        public int? Selenium { get; set; }

        public int? Copper { get; set; }

        public int? Manganese { get; set; }

        public int? Chromium { get; set; }

        public int? Molybdenum { get; set; }

        public int? Chloride { get; set; }

        [Column(TypeName = "text")]
        public string Ingredients { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FoodItemCategory> FoodItemCategories { get; set; }
    }
}

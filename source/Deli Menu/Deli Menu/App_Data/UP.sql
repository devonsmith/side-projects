﻿-- Food items in the menu
CREATE TABLE dbo.FoodItem 
(
	-- Identity information
	ID                 INT IDENTITY(0,1),
	ItemName           NVARCHAR(255) NOT NULL,
	-- Serving information
	ServingSize INT NOT NULL, -- grams
	ServingsContained INT NOT NULL, -- number.
	-- Basic Information
	Calories	       INT NOT NULL, -- grams
	TotalFat	       INT NOT NULL, -- grams
	SaturatedFat       INT, -- grams
	TransFat           INT, -- Should be null, since transfats are illegal.
	Colesterol	       INT NOT NULL, --miligrams
	Sodium             INT NOT NULL, -- miligrams
	TotalCarbohydrate  INT NOT NULL, -- grams
	DietaryFiber       INT NOT NULL, -- grams
	TotalSugars        INT NOT NULL, --grams
	AddedSugars        INT, -- grams
	Protien            INT NOT NULL, --grams

	-- Vitamins
	A                  INT, -- International Unit
	C                  INT, -- miligrams
	Calcuim            INT, -- miligrams
	Iron               INT, -- miligrams
	D                  INT, -- International Unit
	E                  INT, -- miligrams
	K                  INT, -- micrograms
	Thiamin            INT, -- miligrams
	Ribolavin          INT, -- miligrams
	Niacin             INT, -- miligrams
	Folate             INT, -- micrograms
	B12                INT, -- micrograms
	Biotin             INT, -- micrograms
	PantothenicAcid    INT, -- miligrams
	Phosphorus         INT, -- miligrams
	Iodine             INT, -- micrograms
	Magnesium          INT, -- miligrams
	Zinc               INT, -- miligrams 
	Selenium           INT, -- micrograms
	Copper             INT, -- miligrams
	Manganese          INT, -- miligrams
	Chromium           INT, -- micrograms
	Molybdenum         INT, -- micrograms
	Chloride           INT, -- miligrams
	-- Unstructured additional data
	Ingredients TEXT
	-- Constraints
	CONSTRAINT[PK_dbo.FoodItem] PRIMARY KEY CLUSTERED(ID ASC)
);

CREATE TABLE dbo.Category
(
	ID INT IDENTITY(0,1),
	CategoryName NVARCHAR(255) NOT NULL, -- Name of the category
	-- Constraints
	CONSTRAINT[PK_dbo.Category] PRIMARY KEY CLUSTERED(ID ASC)
);


-- Food item and category intersection table.
CREATE TABLE dbo.FoodItemCategory
(
	ID INT IDENTITY(0,1),
	FoodItemID INT, -- The food item in the category.
	FoodCategoryID INT, -- The category for the food item.
	-- Constraints
	CONSTRAINT[PK_dbo.FoodItemCategory] PRIMARY KEY CLUSTERED (ID ASC),
	-- Foreign key for the food item.
	CONSTRAINT[FK_dbo.FoodItem_FoodItemCategory] FOREIGN KEY (FoodItemID)
		REFERENCES dbo.FoodItem (ID)
		ON DELETE CASCADE,
	-- Foreign key for the category
	CONSTRAINT[FK_dbo.FoodCategory_FoodItemCategory] FOREIGN KEY (FoodCategoryID)
		REFERENCES dbo.Category (ID)
		ON DELETE CASCADE

);

-- Test data
INSERT INTO dbo.FoodItem(
         ItemName, 
		 ServingSize,
		 ServingsContained,
		 Calories, 
		 TotalFat,
		 SaturatedFat, 
		 TransFat,
		 Colesterol, 
		 Sodium, 
		 TotalCarbohydrate, 
		 DietaryFiber, 
		 TotalSugars, 
		 AddedSugars, 
		 Protien,
		 C,
		 Ingredients
) VALUES('New Food', 55, 8, 230, 8, 1, 0, 0, 160, 37, 4, 12, 10, 3, 50, 'Stuff, things, and flour' );

INSERT INTO dbo.Category(CategoryName) Values('Vegan');

INSERT INTO dbo.FoodItemCategory(FoodItemID, FoodCategoryID) VALUES(0,0);

-- Test query
SELECT FI.ItemName FROM 
       (dbo.FoodItem as FI JOIN dbo.FoodItemCategory AS FIC 
	          ON FI.ID = FIC.FoodItemID) JOIN dbo.Category AS C 
			  ON FIC.FoodCategoryID = C.ID 
       WHERE (C.CategoryName = 'Vegan');  
